/* MesonPipeline.vala
 *
 * Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class Runner.MesonPipeline : Object, Runner.IPipeline
{
    public bool          is_playing { get; protected set; default = false; }
    public Proton.File   root       { get; protected set; }
    public Runner.Plugin plug       { get; protected set; }

    //  [CCode (has_target=false)]
    public delegate void Step();

    private Runner.ProjectManager pm;
    private string? executable = null;

    private ulong cs_con = 0;
    private uint cur_step = 0;
    // Freaking vala won't let me create a delegate array, so this is just a
    // template from which a few functions extract the length (amount) of steps.
    private uint[] steps = { 1, 2, 3, 5 };

    public MesonPipeline(Runner.ProjectManager pm)
    {
        this.pm = pm;
        this.root = pm.root;
    }

    public void setup(Json.Object root) throws Error
    {
        Json.Node? n;
        string? s = null;

        n = root.get_member("executable");
        if (n != null && (s = n.get_string()) != null)
        {
            assert_(s.has_prefix("_build/") == true,
                "executable must start with '_build/'");
            this.executable = s;
        }

        assert_(FileUtils.test(this.root.path + "/meson.build",
                               FileTest.IS_REGULAR),
                "meson.build doesn't exist or is not a file");
    }

    public void start()
    {
        this.is_playing = true;
        this.cur_step = 0; // Just to make sure

        message("START WAS CALLED");

        this.pm.plug.kill.connect(this.kill);

        this.pm.plug.tab.terminal.reset(true, true);
        this.run_step();
    }

    public void kill()
    {
        if (this.is_playing)
            Posix.kill(this.pm.plug.tab.terminal.pid, Posix.Signal.KILL);
    }

    private void _error(string err)
    {
        this.pm.plug.tab.terminal.feed_child((uint8[])err);
        this.is_playing = false;
        this.cur_step = 0;

        if (this.cs_con != 0)
        {
            this.pm.plug.tab.terminal.disconnect(this.cs_con);
            this.cs_con = 0;
        }

        this.finished(1);
    }

    // Meson execution is simple
    // 1. meson _build
    // 2. ninja -C _build
    // 3. extract info from '_build/meson-info/intro-targets.json'
    // 4. maybe install (might not be needed) - Not implemented yet
    // 5. maybe run executable (there might not be one)

    private void run_step()
    {
        // Emit doing signal
        message("Send doing");
        this.doing("Step %u/%u...".printf(this.cur_step + 1,
                                          this.steps.length));

        this.cs_con = this.pm.plug.tab.terminal.child_exited
                        .connect(this.on_step_done);

        switch (this.cur_step)
        {
            case 0: this.step_one(); break;
            case 1: this.step_two(); break;
            case 2: this.step_three(); break;
            case 3: this.step_five(); break;
        }
    }

    private void step_one()
    {
        this.pm.plug.tab.terminal.spawn("meson _build");
    }

    private void step_two()
    {
        this.pm.plug.tab.terminal.spawn("ninja -C _build");
    }

    private void step_three()
    {
        string?     s = null;
        Proton.File f;

        f = new Proton.File(this.root.path +
                           "/_build/meson-info/intro-targets.json");
        f.read_async.begin(null, (obj, res) => {
            s = f.read_async.end(res);

            if (s == null)
            {
                this._error("Could not read '%s' needed to build project"
                            .printf(f.path));
                return;
            }

            this.process_target(s);
        });
    }

    // There is an issue with this step that if the user kills the process (
    // presses stop) nothing will happen.
    private void process_target(string content)
    {
        Json.Parser p;
        Json.Node?  n;
        Json.Array  arr;

        p = new Json.Parser();
        try
        {
            p.load_from_data(content);
            n = p.get_root();

            assert_(n != null, "intro-targets.json no root");
            assert_(n.get_node_type() == Json.NodeType.ARRAY,
                    "intro-targets.json root is not an array");

            arr = n.get_array();
            assert_(arr.get_length() > 0,
                    "intro-target.json root array has no elements");

            n = arr.get_element(0);
            assert_(n.get_node_type() == Json.NodeType.OBJECT,
                    "intro-targets.json root array[0] is not and object");

            var o = n.get_object();
            // From now on everything is questionable
            n = o.get_member("type");
            string? exec = (n != null) ? n.get_string() : null;

            n = o.get_member("filename");
            string? file = (n != null) ?
                n.get_array().get_element(0).get_string() : null;

            if (file != null && exec == "executable")
                this.executable = file; // Proceed
            else if (this.executable == null)
            {
                this._error("Could not get executable");
                return;
            }

            // This is a hack
            this.on_step_done(this.pm.plug.tab.terminal, 0);
        }
        catch (Error e)
        {
            warning(e.message);
            this._error(e.message);
            return;
        }
    }

    private void step_five()
    {
        this.pm.plug.tab.terminal.reset(true, true);
        this.pm.plug.tab.terminal.spawn(this.executable);
    }

    private void on_step_done(Vte.Terminal _term, int res)
    {
        if (this.cs_con != 0)
            this.pm.plug.tab.terminal.disconnect(this.cs_con);
        this.cs_con = 0;

        if (res != 0)
        {
            message("[Runner] Build failed, exited with %d", res);
            this.cur_step = 0;
            this.is_playing = false;
            this.finished(res);
            return;
        }
        else if (this.cur_step + 1 >= this.steps.length)
        {
            this.cur_step = 0;
            this.is_playing = false;
            this.finished(0);
            return;
        }
        else
        {
            this.cur_step++;
            this.run_step();
        }
    }
}
