/* SimplePipeline.vala
 *
 * Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class Runner.SimplePipeline : Object, Runner.IPipeline
{
    public bool          is_playing { get; protected set; default = false; }
    public Proton.File   root       { get; protected set; }
    public Runner.Plugin plug       { get; protected set; }

    private Runner.ProjectManager pm;
    private string[] commands = {};
    private uint cur_command = 0;
    private ulong ce_con = 0; // child_exited connection handler

    public SimplePipeline(Runner.ProjectManager pm)
    {
        this.pm = pm;
        this.root = pm.root;
    }

    public void setup(Json.Object root) throws Error
    {
        Json.Node? n;
        Json.Array arr;
        uint       arrlen;
        string?    item;

        n = root.get_member("commands");
        assert_(n != null, "key 'commands' is required for simple buildsystem");
        assert_(n.get_node_type() == Json.NodeType.ARRAY,
                "key 'commands' must be an array of strings");

        arr = n.get_array();
        assert_(arr != null, "could not get 'commands' array");

        arrlen = arr.get_length();
        assert_(arrlen > 0, "must have at least one command in 'commands'");

        for (uint i = 0; i < arrlen; i++)
        {
            n = arr.get_element(i);
            item = n.get_string();

            assert_(item != null,
                "%u th command is not a string".printf(i + 1));

            this.commands += item;
        }
    }

    public void start()
    {
        this.is_playing = true;
        this.cur_command = 0; // Just to make sure

        message("START WAS CALLED");

        this.pm.plug.kill.connect(this.kill);

        this.pm.plug.tab.terminal.reset(true, true);
        this.run_command();
    }

    public void kill()
    {
        if (this.is_playing)
            Posix.kill(this.pm.plug.tab.terminal.pid, Posix.Signal.KILL);
    }

    private void run_command()
    {
        // Emit doing signal
        message("Send doing");
        this.doing("Step %u/%u...".printf(this.cur_command + 1,
                                          this.commands.length));

        this.pm.plug.tab.terminal.spawn(this.commands[this.cur_command]);

        this.ce_con = this.pm.plug.tab.terminal.child_exited
                        .connect(this.on_command_finished);
    }

    private void on_command_finished(Vte.Terminal _term, int res)
    {
        this.pm.plug.tab.terminal.disconnect(this.ce_con);

        if (res != 0)
        {
            message("[Runner] Build failed, exited with %d", res);
            this.cur_command = 0;
            this.is_playing = false;
            this.finished(res);
            return;
        }
        else if (this.cur_command + 1 >= this.commands.length)
        {
            this.cur_command = 0;
            this.is_playing = false;
            this.finished(0);
            return;
        }
        else
        {
            this.cur_command++;
            this.run_command();
        }
    }
}
