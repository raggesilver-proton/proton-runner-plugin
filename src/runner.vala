/* runner.vala
 *
 * Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class Runner.Tab : Proton.BottomPanelTab
{
    public Proton.Terminal terminal = null;

    internal Tab(Proton.Window win)
    {
        this.terminal = new Proton.Terminal.no_spawn(win);

        this.content = terminal;
        this.aux_content = null;

        this.name = "runner-tab";
        this.title = "RUNNER";
    }
}

public class Runner.Plugin : Object, Proton.PluginIface
{
    public signal void started();
    public signal void finished();
    public signal void kill();

    public bool                 running { get; private set; default = false; }
    public Gtk.Button           btn     { get; private set; }
    public Tab                  tab     { get; private set; }
    public weak Proton.Window   window  { get; private set; }
    public ProjectManager?      pm      { get; private set; default = null; }

    private Proton.Status       status;

    construct
    {
        btn = new Gtk.Button.from_icon_name("media-playback-start-symbolic",
                                            Gtk.IconSize.MENU);

        btn.get_style_context().add_class("wide-button");
        btn.show();

        btn.clicked.connect(on_btn_click);
    }

    public void do_register(Proton.PluginManager pm)
    {
        window = pm.window;

        window.command_palette.add_command(
            new Proton.Command(window, "Runner", "build", null, () => {
                this.run(true); // This can only run, not kill
            })
        );
    }

    void on_btn_click()
    {
        this.run();
    }

    private void run(bool only_run = false)
    {
        if (this.running && !only_run)
            this.kill();
        else
        {
            // This is very dumb. Creating a new Project every time handles
            // the chance that .proton.json was modified, but still a very dumb
            // way of handling that.
            this.pm = new ProjectManager(this);
            this.pm.scan.begin((obj, res) => {
                if (this.pm.scan.end(res) && this.pm.can_play)
                    this.init_pipeline();
            });
        }
    }

    private void init_pipeline()
    {
        message("INIT PIPELINE");
        var p = this.pm.pipeline;

        {
            this.window.status_box.add_status_show(this.status);
            this.running = true;
            var img = this.btn.get_image() as Gtk.Image;
            img.set_from_icon_name("media-playback-stop-symbolic",
                                    Gtk.IconSize.MENU);
        }

        // Connect signals
        p.doing.connect((str) => {
            message("[Runner] Pipeline: %s", str);
        });

        p.finished.connect((res) => {
            this.tab.terminal.feed(
                "\033[1m\n> '%s' exited with code %d <\033[0m".printf(
                this.window.root.name, res).data
            );
            this.window.status_box.remove_status(this.status);
            this.running = false;
            var img = this.btn.get_image() as Gtk.Image;
            img.set_from_icon_name("media-playback-start-symbolic",
                                   Gtk.IconSize.MENU);
        });

        if (!Proton.settings.bottom_panel_visible)
            Proton.settings.bottom_panel_visible = true;
        this.tab.focus_tab();
        p.start();
    }

    public void activate()
    {
        tab = new Tab(window);

        window.left_hb_box.pack_start(btn);
        window.left_hb_box.reorder_child(btn, 0);

        window.bottom_panel.add_tab(tab);

        this.status = new Proton.Status(() => {
            return @"Running $(window.root.name)...";
        }, Proton.StatusBox.Priority.MEDIUM);
    }

    public void deactivate()
    {
        // FIXME not yet implemented
        tab.content.get_parent().remove(tab.content);
        window.left_hb_box.remove(btn);
    }
}

public Type register_plugin(Module module)
{
    return (typeof(Runner.Plugin));
}

