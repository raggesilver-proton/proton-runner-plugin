# Runner plugin for [Proton](https://gitlab.com/raggesilver-proton/proton)

This plugin adds compiling and running features to Proton.

Runner works by reading a file `.proton.json` from the root of the open project.
In order for Runner to work a few keys are required:

```json
{
    "buildsystem": "", // One of ["simple", "meson"]
}
```

For `buildsystem: meson` nothing else is required. It will compile your code
then search and run your executable.

> **Important note**: meson projects are built in `_build`

For `buildsystem: simple` one more key is necessary:

```json
{
    // ...,
    "commands": [
        "first command",
        "second command",
        // ...
    ]
}
```

Simple buildsystem will run commands one-by-one and stop if any of them fail.

## TODO
- Better integration with Proton.StatusBox
- Separate Proton.BottomPanel tabs for build and run
- Automatic buildsystem identification
- Interface for setting up the project
- Give support to more build systems (npm, make)
- Add support for flatpak identification
